//
//  FlowController.swift
//  MVVMC
//
//  Created by Chinh on 29/04/2024.
//

import UIKit

protocol FlowController: UIViewController {

    associatedtype Destination

    // Navigate to different screen based on defined destination
    func navigate(to destination: Destination)

    // Initial screen
    func start()
}
