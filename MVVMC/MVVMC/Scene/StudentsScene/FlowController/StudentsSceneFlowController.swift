//
//  StudentsViewController.swift
//  MVVMC
//
//  Created by Chinh on 29/04/2024.
//

import UIKit

final class StudentsSceneFlowController: UIViewController, FlowController {
    
    enum Destination {
        case studentListing
        case studentDetail
    }
    
    init(dependency: StudentSceneDependencyContainer, navigation: UINavigationController) {
        self.dependency = dependency
        self.navigation = navigation
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let dependency: StudentSceneDependencyContainer

    private let navigation: UINavigationController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        start()
    }
    
    func start() {
        let firstViewController = StudentListingViewController()
        navigation.pushViewController(firstViewController, animated: false)
    }
    
    func navigate(to destination: Destination) {
        
    }
}
