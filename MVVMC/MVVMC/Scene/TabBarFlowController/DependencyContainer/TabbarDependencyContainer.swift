//
//  DependencyContainer.swift
//  MVVMC
//
//  Created by Chinh on 29/04/2024.
//

import UIKit

struct TabbarDependencyContainer {

    static func make() -> TabbarDependencyContainer {
        return TabbarDependencyContainer()
    }
}

protocol SchoolListingControllerFactory {

    func makeSchoolListingViewController() -> SchoolsViewController
}

protocol StudentListingControllerFactory {

    func makeStudentsFlowController(dependency: StudentSceneDependencyContainer,navigation: UINavigationController) -> StudentsSceneFlowController
}


extension TabbarDependencyContainer: SchoolListingControllerFactory {

    func makeSchoolListingViewController() -> SchoolsViewController {
        return SchoolsViewController(nibName: nil, bundle: nil)
    }
}

extension TabbarDependencyContainer: StudentListingControllerFactory {

    func makeStudentsFlowController(dependency: StudentSceneDependencyContainer, navigation: UINavigationController) -> StudentsSceneFlowController {
        StudentsSceneFlowController(dependency: dependency,
                                    navigation: navigation)
    }
}
