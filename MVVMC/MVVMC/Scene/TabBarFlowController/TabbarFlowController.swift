//
//  TabbarFlowController.swift
//  MVVMC
//
//  Created by Chinh on 29/04/2024.
//

import UIKit
import SnapKit

final class TabbarFlowController: UITabBarController, FlowController {

    func navigate(to destination: Destination) {
        
    }

    enum Destination {
        case schoolListing
        case studentListing
    }
    
    private let dependency: TabbarDependencyContainer

    init(dependency: TabbarDependencyContainer = TabbarDependencyContainer.make()) {
        self.dependency = dependency
        super.init(nibName: nil, bundle: nil)
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.backgroundColor = .white
        tabBar.barTintColor = .black
        tabBar.unselectedItemTintColor = .gray
        tabBar.isTranslucent = false
        view.backgroundColor = .white
        start()
    }

    func start() {
        viewControllers = [schoolListingViewController, studentListingViewController]
    }
    
    private func makeViewController(for destination: Destination) -> UIViewController  {
        switch destination {
            
        case .schoolListing:
            let schoolTabBarItem = UITabBarItem(title: "Schools", image: nil, selectedImage: nil)
            let vc = dependency.makeSchoolListingViewController()
            vc.tabBarItem = schoolTabBarItem
            let nav = UINavigationController(rootViewController: vc)
            nav.tabBarItem = schoolTabBarItem
            return nav
        case .studentListing:
            let studentTabBarItem = UITabBarItem(title: "Students", image: nil, selectedImage: nil)
            let navigation = UINavigationController()
            let studentSceneDependency = StudentSceneDependencyContainer.make()
            let flowController = dependency.makeStudentsFlowController(dependency: studentSceneDependency, navigation: navigation)
            
            navigation.tabBarItem = studentTabBarItem
            navigation.setViewControllers([flowController], animated: false)
            return navigation
        }
    }

    private lazy var schoolListingViewController: UIViewController = {
        return makeViewController(for: .schoolListing)
    }()
    
    private lazy var studentListingViewController: UIViewController = {
        return makeViewController(for: .studentListing)
    }()

}
